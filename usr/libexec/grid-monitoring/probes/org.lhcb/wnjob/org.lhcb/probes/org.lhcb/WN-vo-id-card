#!/bin/bash
#
# Test requirements set out in the LHCb VO ID Card:
#   https://operations-portal.egi.eu/vo/view/voname/lhcb
#
# 1) /cvmfs/lhcb.cern.ch or $VO_LHCB_SW_DIR contains directory "lib"
# 2) Warning if /cvmfs/lhcb-condb.cern.ch absent
# 3) 4000MB memory RSS
# 4) 15000MB scratch (in current working directory)
# 5) That the RPM HEP_OSlibs exists and is the current version
#    or above
#

export MEMORY_WARNING=4000
export MEMORY_ERROR=`expr $MEMORY_WARNING - 1000`
export SCRATCH_TEST=15000

# You can see the current versions in http://linuxsoft.cern.ch/wlcg/
export HEP_OSLIBS_SL6=010020 # 1.0.20
export  HEP_OSLIBS_C7=070110 # 7.1.10

Info=0
Warning=1
Error=2

SHORT_OUTPUT="VO ID Card tests passed"
LONG_OUTPUT=""
RET_CODE=0

export SETUP_PATH='lib/lcg/external/gcc/4.8.1/x86_64-slc6/setup.sh'

log() {
        levelname=$1
        output=$2
        eval levelnum=\$$levelname
        
        LONG_OUTPUT="${LONG_OUTPUT}`date --utc +'%Y-%m-%d %H:%M:%S %Z'` ${levelname}: ${output}\n"

        if [ $levelnum -gt $RET_CODE ] ; then
          RET_CODE=$levelnum
          SHORT_OUTPUT="$levelname: $output"
        fi
      }

log 'Info' "Starting tests of LHCb VO ID Card on `hostname`"
log 'Info' 'VO ID Card requirements listed here: https://operations-portal.egi.eu/vo/view/voname/lhcb'

#
# cvmfs tests. We check /cvmfs/lhcb.cern.ch/lib exists
# but use $VO_LHCB_SW_DIR to get the location if it's set.
#
log 'Info' 'Starting lhcb.cern.ch cvmfs test'
        
if [ "$VO_LHCB_SW_DIR" != "" ] ; then
  export CVMFS_PATH="$VO_LHCB_SW_DIR"
  log 'Info' "Testing LHCb cvmfs at \$VO_LHCB_SW_DIR=$CVMFS_PATH"
else
  export CVMFS_PATH='/cvmfs/lhcb.cern.ch'
  log 'Info' "\$VO_LHCB_SW_DIR not defined. Using $CVMFS_PATH"
fi

if [ -r "$CVMFS_PATH/$SETUP_PATH" ] ; then
  log 'Info' "$CVMFS_PATH/$SETUP_PATH is readable"
  log 'Info' 'Passed cvmfs test'
else
  log 'Error' "$CVMFS_PATH/$SETUP_PATH does not exist!"
fi

#
# Check /cvmfs/lhcb-condb.cern.ch/lib exists but only report
# a warning if it is absent.
#
log 'Info' 'Starting lhcb-condb.cern.ch cvmfs test'
        
if [ -r /cvmfs/lhcb-condb.cern.ch/git-conddb ] ; then
  log 'Info' "/cvmfs/lhcb-condb.cern.ch/git-conddb is readable"
  log 'Info' 'Passed lhcb-condb.cern.ch cvmfs test'
else
  log 'Warning' "/cvmfs/lhcb-condb.cern.ch/git-conddb does not exist!"
fi

# 
# RSS memory test. Memory MB=1024^2. 
# Try to provoke cgroups or ulimit etc to prevent memorytest.
# Assume 4096 is the page size: dirty each page with 1 byte.
# Skip allocating first 20MB to allow for memorytest itself.
# (Set limit with  ulimit -v  if retest of this script needed.)
#
log 'Info' 'Starting memory test'

(
. $CVMFS_PATH/$SETUP_PATH $CVMFS_PATH/lib/lcg/external
cat <<EOF | gcc -o memorytest -xc -
#include <stdio.h>
#include <malloc.h>
int main() { int i; char *p;
for (i = 20 * 256; (i <= $MEMORY_WARNING * 256) && ((p=malloc(4096)) != NULL); ++i) *p = '1';
printf("%d\n", i / 256); return p == NULL;}
EOF
)

if [ ! -x memorytest ] ; then
  log 'Warning' 'Failed to build memorytest executable for memory test'
else
  allocated=`./memorytest`
  rm -f memorytest
  if [ "$allocated" == "$MEMORY_WARNING" ] ; then
    log 'Info' "Passed ${MEMORY_WARNING}MB memory test"
  elif [ "$allocated" != "" -a "$allocated" -lt $MEMORY_ERROR ] ; then
    log 'Error' "Failed to allocate ${MEMORY_WARNING}MB of memory! Stopped at ${allocated}MB"
  elif [ "$allocated" != "" ] ; then
    log 'Warning' "Failed to allocate ${MEMORY_WARNING}MB of memory! Stopped at ${allocated}MB"
  else
    log 'Error' "Failed to allocate ${MEMORY_WARNING}MB of memory! memorytest process killed?"
  fi
fi

#
# Scratch space size (disk MB=1000^2)
# We require that we are already in the scratch area.
#
log 'Info' 'Starting scratch disk test'

freeblocks=`stat -f --format '%b' .`
blockbytes=`stat -f --format '%S' .`
megabytes=`expr $freeblocks \* $blockbytes / 1000000`

if [ "$megabytes" -lt $SCRATCH_TEST ] ; then
  log 'Error' "Only ${megabytes}MB of scratch disk - not ${SCRATCH_TEST}MB!"
else
  log 'Info' "Passed ${SCRATCH_TEST}MB scratch disk test (${megabytes}MB)"
fi

#
# HEP_OSlibs test 
#
log 'Info' 'Starting HEP_OSlibs test'

rpm --version >/dev/null 2>&1
if [ $? = 0 ] ; then
  rpm -qa | grep '^HEP_OSlibs-' >/dev/null
  if [ $? = 0 ] ; then
    # CentOS7
    export spaces_version=`rpm -qa | grep '^HEP_OSlibs-' | head -1 | cut -f2 -d- | sed 's/\./ /g'`
    export nospaces_version=`printf '%02d%02d%02d' $spaces_version`

    if [ $nospaces_version -ge $HEP_OSLIBS_C7 ] ; then
      log 'Info' "Recent CentOS7 version ($nospaces_version >= $HEP_OSLIBS_C7) of HEP_OSlibs RPM is installed"
    else
      log 'Warning' "HEP_OSlibs for CentOS7 is out of date! ($nospaces_version < $HEP_OSLIBS_C7)"
    fi
  else
    rpm -qa | grep '^HEP_OSlibs_SL6-' >/dev/null
    if [ $? = 0 ] ; then
      # SL6
      export spaces_version=`rpm -qa | grep '^HEP_OSlibs_SL6-' | head -1 | cut -f2 -d- | sed 's/\./ /g'`
      export nospaces_version=`printf '%02d%02d%02d' $spaces_version`
  
      if [ $nospaces_version -ge $HEP_OSLIBS_SL6 ] ; then
        log 'Info' "Recent SL6 version ($nospaces_version >= $HEP_OSLIBS_SL6) of HEP_OSlibs RPM is installed"
      else
        log 'Warning' "HEP_OSlibs for SL6 is out of date! ($nospaces_version < $HEP_OSLIBS_SL6)"
      fi
    else
      log 'Warning' "HEP_OSlibs is not installed!"
    fi   
  fi
else
  log 'Info' 'rpm command is not available, checking individual packages'
fi

# HEP_OSlibs checks: git 
git version >/dev/null 2>&1
if [ $? = 0 ] ; then
  log 'Info' 'HEP_OSlibs checks: git installed ok'
else
  log 'Warning' 'HEP_OSlibs checks: git is required but not installed!'
fi

#
# Output final messages and return code
#
log 'Info' 'End of LHCb VO ID Card tests'

/bin/echo    "$SHORT_OUTPUT"
/bin/echo -e "$LONG_OUTPUT"
exit $RET_CODE
